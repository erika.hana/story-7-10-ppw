from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from .views import bookshelves, retrieve, landingPage, accordion, signupFunc, loginFunc, logOut

# Create your tests here.


class Story7Test(TestCase):

    def setUp(self):
        self.client = Client()

    # test for story7

    def test_landing_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landingPage.html')

    def test_landing_page_url(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_landing_page_content(self):
        response = Client().get('')
        self.assertIn("Stories", response.content.decode('utf8'))

    def test_landing_page_views(self):
        found = resolve('/')
        self.assertEqual(found.func, landingPage)

    def test_accordion_template(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, 'accordion.html')

    def test_accordion_url(self):
        response = Client().get('/accordion/')
        self.assertEquals(response.status_code, 200)

    def test_accordion_content(self):
        response = Client().get('/accordion/')
        self.assertIn("Achievements", response.content.decode('utf8'))

    def test_views_accordion(self):
        found = resolve('/accordion/')
        self.assertEqual(found.func, accordion)

    # test for story8

    def test_bookshelves_template(self):
        response = Client().get('/bookshelves/')
        self.assertTemplateUsed(response, 'bookshelves.html')

    def test_bookshelves_url(self):
        response = Client().get('/bookshelves/')
        self.assertEquals(response.status_code, 200)

    def test_bookshelves_content(self):
        response = Client().get('/bookshelves/')
        self.assertIn("Search", response.content.decode('utf8'))

    def test_views_bookshelves(self):
        found = resolve('/bookshelves/')
        self.assertEqual(found.func, bookshelves)

    def test_views_retrieve(self):
        found = resolve('/bookshelves/api/books/<str:book>/')
        self.assertEqual(found.func, retrieve)

    def test_retrieve_url(self):
        response = Client().get('/bookshelves/api/books/<str:book>/')
        self.assertEquals(response.status_code, 200)

    # test for story 9

    def test_signup_template(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_signup_url(self):
        response = Client().get('/signup/')
        self.assertEquals(response.status_code, 200)

    def test_login_url(self):
        response = Client().get('/login/')
        self.assertEquals(response.status_code, 200)

    def test_signup_page_content(self):
        response = Client().get('/signup/')
        self.assertIn("Sign", response.content.decode('utf8'))

    def test_login_page_content(self):
        response = Client().get('/login/')
        self.assertIn("Username", response.content.decode('utf8'))

    def test_login_views(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginFunc)

    def test_signup_views(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signupFunc)

    def test_login_page_have_form(self):
        request = HttpRequest()
        response = loginFunc(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)

    def test_signup_page_have_form(self):
        request = HttpRequest()
        response = signupFunc(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)

    def test_logout_work(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_login_GET(self):
        response = Client().get('/login/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_signup_GET(self):
        response = Client().get('/signup/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')

    def test_login_POST(self):
        response = self.client.post('/login/', follow=True, data={
            'username': 'rikaari',
            'password': 'rahasiarikarikarika'
        })
        self.assertContains(response, 'rikaari')

    def test_signup_POST(self):
        response = self.client.post('/signup/', follow=True, data={
            'username': 'erikaerika',
            'password1': 'erikacantik123',
            'password2': 'erikacantik123'
        })
        self.assertContains(response, 'erikaerika')

    def test_not_valid_POST(self):
        response = self.client.post('/login/', {
            'usernaaaaameee': 'yuhuw',
            'paaaasword': 'lahlahlah'
        }, follow=True)
        self.assertContains(response, 'Invalid entry')
