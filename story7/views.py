from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import LoginForm, SignUpForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
import json
import requests

# Create your views here.


def landingPage(request):
    return render(request, 'landingPage.html')


def accordion(request):
    return render(request, 'accordion.html')


def bookshelves(request):
    return render(request, 'bookshelves.html')


def retrieve(request, book):
    argReq = requests.get(
        "https://www.googleapis.com/books/v1/volumes?q=" + book)
    data = argReq.json()
    data = json.dumps(data)
    print(data)
    return HttpResponse(content=data, content_type="application/json")

# story 9


def loginFunc(request):
    if request.method == "POST":
        form = LoginForm(data=request.POST)

        if form.is_valid():
            usernameInput = request.POST["username"]
            passwordInput = request.POST["password"]

            print(usernameInput, passwordInput)
            user = authenticate(
                request, username=usernameInput, password=passwordInput)

            if user is not None:
                login(request, user)
                return redirect('story7:login')

        else:
            messages.error(request, 'Invalid entry')

    else:
        form = LoginForm()

    context = {
        "form": form,
    }

    return render(request, 'login.html', context)


def signupFunc(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("story7:login")

        else:
            for msg in form.error_messages:
                messages.error(request, 'Invalid entry')

    else:
        form = SignUpForm()

    context = {
        'form': form
    }

    return render(request, 'signup.html', context)


def logOut(request):
    logout(request)
    print("halo")
    return redirect('story7:login')
