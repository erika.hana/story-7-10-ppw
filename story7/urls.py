from django.urls import path
from . import views

app_name = 'story7'

urlpatterns = [
    path('', views.landingPage, name='landingPage'),
    path('accordion/', views.accordion, name='accordion'),
    path('bookshelves/', views.bookshelves, name='bookshelves'),
    path('bookshelves/api/books/<str:book>/', views.retrieve, name='retrieve'),
    path('signup/', views.signupFunc, name='signup'),
    path('login/', views.loginFunc, name='login'),
    path('logout/', views.logOut, name='logout'),
]
