# Project Story 7-10

This repository is for Perancangan dan Pemrograman Web's Story Project from Story 7 until Story 10.

## Personal Data

- Name : Erika Hana Prasanti
- Student Number : 1906298872
- Class : G

## Story 7 : Client Side Programming

- Implementing Javascript and Jquery by making an Accordion Page
- Link to Story : [Story 7](https://erika-stories.herokuapp.com/accordion/)


## Story 8 : Data Exchange Format

- Implementing AJAX and JSON by making a book-search bar that uses the data from Google API
- Link to Story : [Story 8](https://erika-stories.herokuapp.com/bookshelves/)

## Link to The Whole Website
[erika-stories](https://erika-stories.herokuapp.com/)